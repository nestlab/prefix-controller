# Prefix controller

Prefix controller factory for NestJS.

### Install
```
$ npm i @nestlab/prefix-controller
```

### Examples

**Prefix for application space**

```typescript

const AdminController = createPrefixController('admin');
const ConsoleController = createPrefixController('console');

@AdminController('logs')           // Base URL: /admin/logs
export class LogsController {
  // Your implementation
}

@AdminController('users')          // Base URL: /admin/users
export class UserController {
  // Your implementation
}

@ConsoleController('products')     // Base URL: /console/products
export class ProductController {
  // Your implementation
}

```

**Prefix for versioning**

```typescript
const Version1Controller = createPrefixController('v1');
const Version2Controller = createPrefixController('v2');

@Version1Controller('auth')          // Base URL: /v1/auth
export class AuthController {
  // Your implementation
}

@Version2Controller('auth')          // Base URL: /v2/auth
export class AuthController {
  // Your implementation
}

```


Enjoy!
