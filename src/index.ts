import { Controller } from '@nestjs/common';

export const createPrefixController = (prefix: string) => (path?: string) => Controller([prefix, path].join('/'));
