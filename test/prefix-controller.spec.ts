import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { createPrefixController } from '../src';
import { Get } from '@nestjs/common';

const ApplicationController = createPrefixController('app');

@ApplicationController('test')
class AppController {
    @Get('ok')
    getOk(): string {
        return 'OK';
    }
}

describe('Prefix controller', () => {
    let app: INestApplication;

    beforeAll(async () => {
        const testingModule = await Test.createTestingModule({
            controllers: [AppController]
        }).compile();

        app = testingModule.createNestApplication();
    });

    test('Prefix creation', () => {
        request(app.getHttpServer()).get('/app/test/ok')
            .expect(200, 'OK');
    });
});
